//
//  CollectionViewCell.swift
//  ShopProject
//
//  Created by Admin on 15.07.2022.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let identifier = "CollectionViewCell"
    func config(by item: Item) {
        if let imageURL = item.imageLink {
            DispatchQueue.main.async {
                self.collectionCellImageView.load(url: URL(string: imageURL))
                self.collectionCellImageView.contentMode = .scaleAspectFill
            }
        }
        if collectionCellImageView.image == nil {
            DispatchQueue.main.async {
                self.collectionCellImageView.image = UIImage(systemName: "xmark.icloud.fill")
            }
        }
        if let name = item.name {
            collectionCellNameLabel.text = name
        }
        if let price = item.price, let currency = item.currency {
            collectionCellPriceLabel.text = "\(price) \(currency)"
        }
    }

    let cellView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 14
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let collectionCellImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 14
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    let collectionCellNameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()

    let collectionCellPriceLabel: UILabel = {
        let priceLabel = UILabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        return priceLabel
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func prepareForReuse() {
        collectionCellImageView.image = nil
    }
    func addViews() {
        addSubview(cellView)
        cellView.addSubview(collectionCellImageView)
        cellView.addSubview(collectionCellNameLabel)
        cellView.addSubview(collectionCellPriceLabel)
        NSLayoutConstraint.activate([
            cellView.topAnchor.constraint(equalTo: topAnchor),
            cellView.bottomAnchor.constraint(equalTo: bottomAnchor),
            cellView.trailingAnchor.constraint(equalTo: trailingAnchor),
            cellView.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
        NSLayoutConstraint.activate([
            collectionCellImageView.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 4),
            collectionCellImageView.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: -4),
            collectionCellImageView.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 4),
            collectionCellImageView.bottomAnchor.constraint(equalTo: cellView.bottomAnchor, constant: -45)
        ])
        NSLayoutConstraint.activate([
            collectionCellNameLabel.topAnchor.constraint(equalTo: collectionCellImageView.bottomAnchor, constant: 4),
            collectionCellNameLabel.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: 4),
            collectionCellNameLabel.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 4),
            collectionCellNameLabel.bottomAnchor.constraint(equalTo: collectionCellPriceLabel.topAnchor, constant: -4)
        ])
        NSLayoutConstraint.activate([
            collectionCellPriceLabel.topAnchor.constraint(equalTo: collectionCellNameLabel.bottomAnchor, constant: 4),
            collectionCellPriceLabel.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: 4),
            collectionCellPriceLabel.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 4),
            collectionCellPriceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
    }
}
