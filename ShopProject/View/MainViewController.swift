//
//  ViewController.swift
//  ShopProject
//
//  Created by Admin on 13.07.2022.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    private var collectionView: UICollectionView?
    private let viewModel = ItemsCollectionViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Test Shop"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        //Binding
        viewModel.items.bind { [weak self] _ in
            self?.collectionView?.reloadData()
        }
        viewModel.fetchData()
        collectionViewSetting()
    }
    // MARK: - Collection View Settings
    func collectionViewSetting() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: view.frame.size.width / 2 - 8,
                                 height: view.frame.size.height / 2 - 4)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 1
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        guard let collectionView = collectionView else { return }
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CollectionViewCell.self,
                                forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.frame = view.bounds
        collectionView.alwaysBounceVertical = true
        view.addSubview(collectionView)
    }
    // MARK: - Data Source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.items.value?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as? CollectionViewCell
        let item = viewModel.items.value?[indexPath.row]
        cell?.config(by: item!)
        return cell ?? CollectionViewCell(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
    }
}
