//
//  Router.swift
//  ShopProject
//
//  Created by Admin on 15.07.2022.
//

import Foundation
import Alamofire

protocol URLRequestBuilder: URLRequestConvertible {
    var baseURL: String { get }
//    var path: String? { get }
    var headers: HTTPHeaders? { get }
    var parameters: Parameters? { get }
    var method: HTTPMethod { get }
}

extension URLRequestBuilder {
    var baseURL: String {
        return Constants.baseURL
    }

    func asURLRequest() throws -> URLRequest {
        let url = try baseURL.asURL()

//        var request = URLRequest(url: url.appendingPathComponent(path ?? ""))
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        switch method {
        case .get:
            request.allHTTPHeaderFields = headers?.dictionary
            request = try URLEncoding.default.encode(request, with: parameters)
        default:
            break
        }
        return request
    }
}

enum CategoryProvider: URLRequestBuilder {
    case showLipsticks
    case showEyeliner
    case showBlush

//    var path: String? {
//        switch self {
//        default:
//            return nil
//        }
//    }

    var headers: HTTPHeaders? {
        switch self {
        default:
            return nil
        }
    }

    var parameters: Parameters? {
        switch self {
        case .showLipsticks:
            return ["product_type": "lipstick"]
        case .showEyeliner:
            return ["product_type": "eyeliner"]
        case .showBlush:
            return ["product_type": "blush"]
        }
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
}

enum Result<T: Codable> {
    case success(T)
    case failure(Error)
}

class ServiceProvider<T: URLRequestBuilder> {
    func load<U: Codable>(service: T, decodeType: U.Type, completion: @escaping ((Result<U>) -> Void)) {
        guard let urlRequest = service.urlRequest else { return }
        AF.request(urlRequest).responseDecodable(of: U.self) { (response) in
            switch response.result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
