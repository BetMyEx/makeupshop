import Foundation
// MARK: - ItemElement

typealias Items = [Item]

struct Item: Codable {
//    let id: Int
//    let brand: String
    let name: String?
    let price: String?
//    let priceSign: String
    let currency: String?
    let imageLink: String?
//    let productLink: String
//    let websiteLink: String
//    let itemDescription: String
//    let rating: JSONNull?
//    let category, productType: String
//    let tagList: [String]
//    let createdAt, updatedAt: String
//    let productAPIURL: String
//    let apiFeaturedImage: String
//    let productColors: [ProductColor]

    enum CodingKeys: String, CodingKey {
//        case id
//        case brand
        case name
        case price
//        case priceSign = "price_sign"
        case currency
        case imageLink = "image_link"
//        case productLink = "product_link"
//        case websiteLink = "website_link"
//        case itemDescription = "description"
//        case rating, category
//        case productType = "product_type"
//        case tagList = "tag_list"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case productAPIURL = "product_api_url"
//        case apiFeaturedImage = "api_featured_image"
//        case productColors = "product_colors"
    }
}

// MARK: - ProductColor
// struct ProductColor: Codable {
//    let hexValue, colourName: String
//
//    enum CodingKeys: String, CodingKey {
//        case hexValue = "hex_value"
//        case colourName = "colour_name"
//    }
// }

// MARK: - Encode/decode helpers
//
// class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
// }
