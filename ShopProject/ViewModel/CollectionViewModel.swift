//
//  CollectionViewModel.swift
//  ShopProject
//
//  Created by Admin on 16.07.2022.
//

import UIKit
struct ItemsCollectionViewModel {
    var items: Observable<[Item]> = Observable([])
    let serviceProvider = ServiceProvider<CategoryProvider>()
    func fetchData() {
        DispatchQueue.global().sync {
            serviceProvider.load(service: .showLipsticks, decodeType: Items.self) { (result) in
                switch result {
                case .success(let response):
                    self.items.value = response
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
